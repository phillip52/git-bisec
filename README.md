
# git-bisect 
### Murder Mystery

[git documents on bisect](https://git-scm.com/docs/git-bisect)

# Fun and interactive way to teach Git bisect
Use binary search to find the commit that introduced a bug

- Your task is to use Git bisect to help the police identify the killer by narrowing down the list of suspects based on the evidence.


